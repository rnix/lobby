<?php

namespace Lobby;

interface GameInterface {
    
    /**
     * Perfoms command from player.
     * 
     * @param StdClass $input - contains data recieved from client
     * @param \Lobby\Player $player - command sender
     */
    public function onCommand($input, Player $player);
    
}