<?php

namespace Lobby;

class Room {
    public $name;
    protected $ownerPlayer;
    public $id;
    public $members;
    public $ownerId;
    
    public function __construct($player, $name = null) {
        $this->ownerPlayer = $player;
        $this->ownerId = $player->getId();
        $this->name = \CHtml::encode($name);
        
        $this->id = md5($player->getConnection()->getId() . $this->name . time());
        $this->addMember($player);
    }
    
    public function getOwner(){
        return $this->ownerPlayer;
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function addMember($player){
        $this->members[$player->uid] = $player;
    }
    
    public function removeMember($player){
        unset($this->members[$player->uid]);
    }
}