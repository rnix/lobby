<?php

namespace Lobby;

class Player {
    protected $connection;
    public $uid;
    protected $model;
    public $name;
    
    public function __construct($connection, $uid) {
        $this->connection = $connection;
        $this->uid = $uid;
        
        $this->model = \User::model()->findByPk($uid);
        if ($this->model){
            $this->name = \CHtml::encode($this->model->name);
        }
    }
    
    public function getConnection(){
        return $this->connection;
    }
    
    public function getModel(){
        return $this->model;
    }
    
    /**
     * Sends command to client connection
     * 
     * @param \Lobby\DataFrame $df
     */
    public function send(DataFrame $df){
        $connection = $this->connection;
        if ($connection){
            $socket = $connection->getSocket();
            if ($socket && $socket->isConnected()){
                $connection->send($df);
            }
        }
    }
    
    public function getId(){
        return $this->uid;
    }
    
    public function getName(){
        return $this->name;
    }
    
}