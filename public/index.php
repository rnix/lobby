<?php

/**
 * Make file APPLICATION_ENV.php in DOCUMENT_ROOT with following content:
 * <?php return 'dev_home';
 * 
 * 'dev_home' - value of constant APPLICATION_ENV. 
 * Add appropriate case to switch below.
 */
if (file_exists('APPLICATION_ENV.php')) {
    defined('APPLICATION_ENV') || define('APPLICATION_ENV', include 'APPLICATION_ENV.php');
} else if (getenv('APPLICATION_ENV')) { 
    defined('APPLICATION_ENV') || define('APPLICATION_ENV', getenv('APPLICATION_ENV'));
} else {
    echo "PUT FILE WITH NAME 'APPLICATION_ENV.php' INTO public DIRECTORY WHICH RETURNS VALUE OF APPLICATION_ENV. SEE " . __FILE__;
    return;
}

$configDir = dirname(__FILE__) . '/../protected/config';

switch (APPLICATION_ENV) {
    case 'dev_home':
        $yii = dirname(__FILE__) . '/../../yii-1.1.13/framework/yii.php';
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
        break;
    case 'dev_work':
        $yii = dirname(__FILE__) . '/../../../../distr/yii-1.1.13/framework/yii.php';
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('YII_ENABLE_ERROR_HANDLER') or define('YII_ENABLE_ERROR_HANDLER', false);
        break;
    case 'production':
        $yii = '/usr/share/php/yii-1.1.13/framework/yii.php';
        defined('YII_DEBUG') or define('YII_DEBUG', false);
        break;
    default:
        throw new Exception("VALUE '" . APPLICATION_ENV . "' OF APPLICATION_ENV IS NOT SUPPORTED.");
        break;
}

defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);

$configMain = require_once( $configDir . '/main.php' );
$configServer = require_once( $configDir . '/server.' . APPLICATION_ENV . '.php' );
$config = CMap::mergeArray($configMain, $configServer);

Yii::createWebApplication($config)->run();