<?php
/* edit and rename this file to server.%APP_ENV%.php */
return array(
    'params' => array(
        'webSocketAddress' => 'lobby.lh:8080',
        'webSocketServerPort' => '8080',
        'webSocketOptions' => array(
            'allowed_origins' => array('lobby.lh'),
        ),
        'livelevelAchs' => array(
            'one_victory' => 0,
        ),
    ),
    'components' => array(
        'db' => array(
            'connectionString' => 'mysql:host=localhost;dbname=maze',
            'emulatePrepare' => true,
            'username' => 'maze_user',
            'password' => 'maze_pass',
        ),
        'eauth' => array(
            'services' => array(
                'livelevel' => array(
                    'class' => 'LivelevelOAuthService',
                    'client_id' => '',
                    'client_secret' => '',
                ),
            ),
        ),
    )
);
