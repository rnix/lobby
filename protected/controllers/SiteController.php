<?php

class SiteController extends Controller {

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    public function actionIndex(){
        $this->render('index');
    }
    
    public function actionMaze() {
        $this->render('maze');
    }
    
    public function actionLogin() {
        
        if (!empty($_GET['error'])){
            throw new CException($_GET['error']);
        }
                
        $authIdentity = Yii::app()->eauth->getIdentity('livelevel');
        $authIdentity->redirectUrl = Yii::app()->user->returnUrl;
        $authIdentity->cancelUrl = $this->createAbsoluteUrl('site/login');

        if ($authIdentity->authenticate()) {
            $identity = new MyEAuthUserIdentity($authIdentity);
            
            if ($identity->authenticate()) {
                $duration = 3600 * 24 * 30; // 30 days
                Yii::app()->user->login($identity, $duration);
                
                // special redirect with closing popup window
                $authIdentity->redirect();
            } else {
                // close popup window and redirect to cancelUrl
                $authIdentity->cancel();
            }
        }

        // Something went wrong, redirect to login page
        $this->redirect(array('site/index'));
    }
    
    public function actionLogout(){
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionAnonLogin(){
        if (Yii::app()->user->isGuest){
            Yii::app()->user->loginAsAnonymous();
        }
        $this->redirect(Yii::app()->user->returnUrl);
    }
    
    public function actionSelectLogin(){
        $this->render('selectLogin');
    }
}