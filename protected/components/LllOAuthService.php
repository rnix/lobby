<?php

/**
 * Livelevel provider class for debug.
 */
class LllOAuthService extends LivelevelOAuthService {

    protected $providerOptions = array(
        'authorize' => 'http://lll.lh/oauth/authorize',
        'access_token' => 'http://lll.lh/oauth/access_token',
    );

    protected function fetchAttributes() {
        $info = $this->makeSignedRequest('http://lll.lh/api/users/info');
        if ($info) {
            $this->attributes['id'] = $info->id;
            $this->attributes['name'] = $info->screen_name;
            $this->attributes['url'] = 'http://lll.lh/user/' . $info->screen_name;
        }
    }

}