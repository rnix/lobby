<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name . ' - Menu';
?>

<?php

?>
<div class="row">

    <div class="alert alert-info connection-wait">
        <h3>Происходит подключение</h3>
        <div class="progress progress-striped active">
            <div class="bar" style="width: 100%;"></div>
        </div>
    </div>
    
    <div class="alert alert-danger connection-error" style="display: none;">
        Не удалось подключиться к серверу.
    </div>

    <div class="span12">
        <div class="rooms-list-container" style="display: none;"> 
            <h4>Список комнат</h4>
            <em class="empty-rooms-list-message" style="display: none;">ни одной комнаты не создано</em>
            <ul class="rooms-list">
                <li class="room-item-tpl" style="display: none;">
                    <span class="room-item-name">name</span>
                </li>
            </ul>
            
            <input type="text" name="roomName"><br><button class="btn createRoom">Create</button>
        </div>
        
        <div class="players-list-container" style="display: none;"> 
            <h4>Список игроков в комнате <span class="room-name"></span></h4>
            <em class="room-is-empty-message" style="display: none;">комната пуста</em>
            <ul class="players-list">
                <li class="player-item-tpl" style="display: none;">
                    <span class="player-item-name">name</span>
                </li>
            </ul>
            
            <br><button class="btn leaveRoom">Leave room</button>
            <div class="start-game-container" style="display: none;">
                <br><button class="btn startGame">Start</button>
            </div>
        </div>
        
        <div class="game-info-container" style="display: none;">
            <div class="alert alert-success">Игра запущена. Id = <span class="game-info-id">?</span></div>
            <button class="btn exampleAction">Example action</button>
            <br><button class="btn leaveGame">Leave game</button>
        </div>
        
        <div class="room-chat-container" style="display: none;">
            <div class="chat-lines">
                <div class="chat-message chat-message-tpl" style="display: none;"><span class="chat-author">author</span>: <span class="chat-text">text</span></div>
            </div>
            <input type="text" class="input-chat-text">
            <button class="btn chat-send">Send</button>
        </div>
        
    </div>
    

</div>