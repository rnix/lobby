<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class="hero-unit">
    <h1>Добро пожаловать на <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
    <p>
        Описание проекта.
    </p>
    <p>
        <a class="btn btn-primary btn-large" href="<?= $this->createUrl('lobby/menu') ?>">
            Играть
        </a>
    </p>
</div>