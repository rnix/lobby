Lobby
==========================

###Описание
Lobby - приложение для Yii, которое содержит серверную и клиентскую 
часть игрового холла с созданием комнат, аутентификацией и прочим.
Используется для создания многопользовательских браузерных игр.
Клиент-серверное взаимодействие осуществляется через Web Socket 
с использованием Wrench.

###Настройка окружения
* Создать базу данных, используя схему из проекта для MySQL Workbench CE,
который расположен в other/schema.mwb
* В дерикториях public и protected создать файл с именем APPLICATION_ENV.php,
который возвращает значение переменной окружения, например 'dev_home':

        <?php return 'dev_home';

* Настроить путь к Yii framework для соответствующего case в файлах
public/index.php (для веб) и protected/yiic.php (для серверной части web socket)
* Заполнить и переименовать config/example_server.php в server.%APP_ENV%.php, например,
server.dev_home.php 

###Создание игры
Вся серверная часть управляется классами, которые находятся в директории app/.
Запуск сервера происходит в файле app/server.php.
Серверная часть игры может состоять из нескольких классов, но точкой входа
является тот, имя которого передано аргументом в конструктор LobbyApplication
(см. файл server.php, строка с $server->registerApplication).

Пример класса с серверной частью игры находится в файле app\GameExample.php
Для разработки своего класса следует наследовать класс \Lobby\GameAbstract
и реализовать \Lobby\GameInterface.

Так как мы запускаем консольное приложение через Yii, то в серверной части
класс Player имеет метод getModel(), который возвращает Yii Active Record
таблицы пользователя.

Пример взаимодействия серверной части с клиентом смотрите в app\GameExample.php.
Пример взаимодействия клиентской части с сервером смотрите в 
protected\views\assets\lobby\js\connection.js.

Работа с Web Socket в клиентской части происходит в файле jquery.lobby.js.
Важной частью этого взаимодействия является аргумент requestHandler.
Он является объектом javascript, который следует расширять для приёма
сообщений с сервера.
Например:

    requestHandler.prototype.onUpdateRoomMembers = function(members){
        updatePlayersList(members);
    };

Другие примеры смотрите в connection.js.



###Запуск
Запустить сервер, который будет слушать web socket соединения:

    php protected/yiic.php server


###Лицензия MIT
    The MIT License

    Copyright (C) 2013 Rnix Valentine

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.


